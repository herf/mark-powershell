using namespace System.Management.Automation

$MARKPATH="$HOME\.mark"
$DBFILE="db.json"
$DBPATH = "$MARKPATH\$DBFILE"

function Mark-Details-Create-Path()
{
    if(-Not (Test-Path $MARKPATH))
    {
        New-Item -Path $MARKPATH -ItemType Directory | Out-Null
    }
    if(-Not (Test-Path $DBPATH))
    {
        New-Item -Path $MARKPATH -Name $DBFILE -ItemType File | Out-Null
        # Create an empty JSON, so we can parse it into an empty custom object later
        Out-File -FilePath $DBPATH -InputObject "{}"
    }
}

function Mark-Details-Read-File()
{
    # Read the file
    $Data = Get-Content $DBPATH | Out-String
    # Convert Json into an actual object
    return ConvertFrom-Json -InputObject $Data
}

function Mark-Details-Write-File([PSCustomObject] $Data)
{
    # Convert back to Json format
    $Data = ConvertTo-Json -InputObject $Data
    # Save the file
    Out-File -FilePath $DBPATH -InputObject $Data
}

Mark-Details-Create-Path

function Mark-Location([String] $Name, [String] $Location = (Get-Location).Path, [switch] $Force = $false)
{
    $Data = Mark-Details-Read-File
    $Member = Get-Member -InputObject $Data -Name $Name

    # Check if there is already a mark with this name
    if(-Not ($Member -eq $Null))
    {
        $Value = $Data.$Name
        $Message = "Mark $Name already defined as $Value"
        
        if(-Not $Force)
        {
            Write-Error -Message $Message

            return
        }
        Write-Warning -Message $Message
    }
    # Add a new member to the data object, with the location
    Add-Member -InputObject $Data -NotePropertyName $Name -NotePropertyValue $Location -Force
    Mark-Details-Write-File -data $Data
}

function Unmark-Location([String] $Name)
{
    $Data = Mark-Details-Read-File
    # Remove the Property with the given name - If there is no such property it will do nothing
    $Data = Select-Object -InputObject $Data -Property *  -ExcludeProperty $Name
    Mark-Details-Write-File -data $Data

}

function Jump-To-Mark([String] $Name)
{
    $Data = Mark-Details-Read-File
    $Value = $Data.$Name

    if($Value -eq $Null)
    {
        Write-Error "No mark specified with name $Name"

        return
    }
    

    Set-Location $Value
}

function List-Marks()
{
    $Data = Mark-Details-Read-File
    Write-Output -InputObject $Data
}
